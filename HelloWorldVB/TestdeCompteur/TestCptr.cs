﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HelloWorldLib;
namespace TestdeCompteur
{
    [TestClass]
    public class TestCptr
    {
        [TestMethod]
        public void TestIncrement()
        {
            compteur incremente = new compteur(0);
            incremente = 0;
            Assert.AreEqual(1, incremente);
        }
        [TestMethod]
        public void TestDecrement()
        {
            compteur decremente = new compteur(0);
            decremente = 0;
            Assert.AreEqual(1, decremente);
        }
    }
}
