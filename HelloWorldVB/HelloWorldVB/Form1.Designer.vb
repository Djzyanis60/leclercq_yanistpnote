﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_decremente = New System.Windows.Forms.Button()
        Me.btn_incremente = New System.Windows.Forms.Button()
        Me.lbl_total = New System.Windows.Forms.Label()
        Me.btn_raz = New System.Windows.Forms.Button()
        Me.lbl_inutile = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btn_decremente
        '
        Me.btn_decremente.Location = New System.Drawing.Point(12, 85)
        Me.btn_decremente.Name = "btn_decremente"
        Me.btn_decremente.Size = New System.Drawing.Size(110, 39)
        Me.btn_decremente.TabIndex = 0
        Me.btn_decremente.Text = "-"
        Me.btn_decremente.UseVisualStyleBackColor = True
        '
        'btn_incremente
        '
        Me.btn_incremente.Location = New System.Drawing.Point(257, 85)
        Me.btn_incremente.Name = "btn_incremente"
        Me.btn_incremente.Size = New System.Drawing.Size(110, 39)
        Me.btn_incremente.TabIndex = 2
        Me.btn_incremente.Text = "+"
        Me.btn_incremente.UseVisualStyleBackColor = True
        '
        'lbl_total
        '
        Me.lbl_total.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_total.AutoSize = True
        Me.lbl_total.Location = New System.Drawing.Point(180, 96)
        Me.lbl_total.Name = "lbl_total"
        Me.lbl_total.Size = New System.Drawing.Size(16, 17)
        Me.lbl_total.TabIndex = 3
        Me.lbl_total.Text = "0"
        '
        'btn_raz
        '
        Me.btn_raz.Location = New System.Drawing.Point(132, 154)
        Me.btn_raz.Name = "btn_raz"
        Me.btn_raz.Size = New System.Drawing.Size(119, 23)
        Me.btn_raz.TabIndex = 4
        Me.btn_raz.Text = "RAZ"
        Me.btn_raz.UseVisualStyleBackColor = True
        '
        'lbl_inutile
        '
        Me.lbl_inutile.AutoSize = True
        Me.lbl_inutile.Location = New System.Drawing.Point(168, 44)
        Me.lbl_inutile.Name = "lbl_inutile"
        Me.lbl_inutile.Size = New System.Drawing.Size(40, 17)
        Me.lbl_inutile.TabIndex = 5
        Me.lbl_inutile.Text = "Total"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(379, 254)
        Me.Controls.Add(Me.lbl_inutile)
        Me.Controls.Add(Me.btn_raz)
        Me.Controls.Add(Me.lbl_total)
        Me.Controls.Add(Me.btn_incremente)
        Me.Controls.Add(Me.btn_decremente)
        Me.Name = "Form1"
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_decremente As Button
    Friend WithEvents btn_incremente As Button
    Friend WithEvents lbl_total As Label
    Friend WithEvents btn_raz As Button
    Friend WithEvents lbl_inutile As Label
End Class
